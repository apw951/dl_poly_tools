### Some tools for DL_POLY related work

___

[Catscan](https://gitlab.com/apw951/dl_poly_tools/-/tree/main/tools/catscan?ref_type=heads) - visualise slices of a DL_POLY config file, utilising CPU or GPU compute

For example, here is a 100,000,000 Argon simulation:

![an example zslice](https://gitlab.com/apw951/dl_poly_tools/-/raw/main/doc/zslice-137.png)

___
