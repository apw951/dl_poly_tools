#ifndef CUDA
#define CUDA

// https://stackoverflow.com/questions/14038589/what-is-the-canonical-way-to-check-for-errors-using-the-cuda-runtime-api
// Usages:
//  gpuErrchk( cudaPeekAtLastError() );
//  gpuErrchk( SOME_CUDA_COMMAND() );
#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

inline void CUDAOK() { gpuErrchk(cudaPeekAtLastError()); }

#endif /* CUDA */
