#ifndef FFT_H
#define FFT_H

#include <cstdint>
#include <vector>
#include <complex>

#include <fftw3.h>

void fft(std::vector<std::complex<double>> & in, std::vector<std::complex<double>> & out)
{
    auto plan = fftw_plan_dft_1d
    (
        out.size(),
        reinterpret_cast<fftw_complex*>(in.data()),
        reinterpret_cast<fftw_complex*>(out.data()),
        FFTW_FORWARD,
        FFTW_ESTIMATE
    );
    fftw_execute(plan);
    fftw_destroy_plan(plan);
}

void ifft(std::vector<std::complex<double>> & in, std::vector<std::complex<double>> & out)
{
    auto plan = fftw_plan_dft_1d
    (
        out.size(),
        reinterpret_cast<fftw_complex*>(in.data()),
        reinterpret_cast<fftw_complex*>(out.data()),
        FFTW_BACKWARD,
        FFTW_ESTIMATE
    );
    fftw_execute(plan);
    fftw_destroy_plan(plan);
}

std::vector<std::complex<double>> realToComplex(const std::vector<double> & rdata)
{
    std::vector<std::complex<double>> complex;
    complex.reserve(rdata.size());
    for (const double & r : rdata){ complex.push_back({r, 0.0}); }
    return complex;
}
#endif