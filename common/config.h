#ifndef CONFIG
#define CONFIG

#include <vector>
#include <fstream>
#include <sstream>
#include <string>
#include <iostream>
#include <cstdint>
#include <chrono>
#include <cmath>
#include <map>

namespace CONFIG
{

    struct Site
    {
        Site() = default;

        Site(double x, double y, double z, double r, uint64_t i)
        : x(x), y(y), z(z), r(r), index(i)
        {}

        double x;
        double y;
        double z;
        double r;
        uint64_t index;
    };

    std::vector<Site> parseCONFIG(std::string path, double r) 
    {
        std::ifstream config(path);
        if (config.is_open())
        {
            auto tic = std::chrono::high_resolution_clock::now();
            std::string line, title, params, cella, cellb, cellc;

            std::getline(config, title);
            std::getline(config, params);
            std::getline(config, cella);
            std::getline(config, cellb);
            std::getline(config, cellc);

            std::stringstream sp(params);
            unsigned levcon, imcon;
            uint64_t natms;
            sp >> levcon >> imcon >> natms;
            uint8_t footerCount = levcon;
            uint64_t atom = 0;

            std::cout << "Parsing:\n"
                    << "  title: " + title + "\n"
                    << "  atoms: " + std::to_string(natms) + "\n"
                    << "  level: " + std::to_string(levcon) + " | 0: pos, 1: pos-vel, 2: pos-vel-force\n";

            std::vector<Site> atoms;

            atoms.resize(natms);

            while (!config.eof())
            {
                std::getline(config, line);
                std::getline(config, line);
                std::stringstream ss(line);
                double x, y, z;
                ss >> x >> y >> z;

                atoms[atom] = Site(x,y,z,r,atom);
                
                
                if (atom == natms-1)
                {
                    break;
                }

                for (uint8_t i = 0; i < footerCount; i++)
                {
                    if (config.eof())
                    {
                        std::cout << "Unexpected end of file reading footer for atom: " + std::to_string(atom+1) + "\n";
                        return {};
                    }
                    std::getline(config, line);
                }
                atom++;
            }
            auto toc = std::chrono::high_resolution_clock::now();
            auto taken = std::chrono::duration<double>(toc-tic).count();
            std::cout << "Read in " << taken << "s " << taken/natms << "s/atom\n";

            return atoms;
        }
        else
        {
            std::cout << "Error opening " + path + "\n";
            return {};
        }

        config.close();

        return {};
    }
}

#endif /* CONFIG */
