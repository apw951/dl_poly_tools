#ifndef CORRELATE_H
#define CORRELATE_H

#include <cstdint>
#include <complex>
#include <vector>

#include <fft.h>

std::vector<double> correlate(const std::vector<double> & adata, const std::vector<double> & bdata)
{
    std::vector<std::complex<double>> ca = realToComplex(adata);
    std::vector<std::complex<double>> cb = realToComplex(bdata);
    std::vector<std::complex<double>> a(2*adata.size(), 0);
    std::vector<std::complex<double>> b(2*adata.size(), 0);
    for (uint64_t i = 0; i < adata.size(); i++)
    {
        a[i] = ca[i];
        b[i+adata.size()] = cb[i];
    }

    std::vector<std::complex<double>> outx(2*ca.size(), 0.0);
    std::vector<std::complex<double>> outy(2*ca.size(), 0.0);

    fft(a, outx);
    fft(b, outy);

    for (uint64_t i = 0; i < outx.size(); i++)
    {
        outx[i] = std::conj(outx[i])*outy[i];
    }

    ifft(outx, outy);

    std::vector<double> out(adata.size(), 0);

    for (uint64_t i = 0; i < adata.size(); i++)
    {
        out[i] = outy[i+adata.size()].real()/float(adata.size()-i);
    }
    return out;
}

std::vector<double> autocorrelate(const std::vector<double> & data) { return correlate(data, data); }


#endif