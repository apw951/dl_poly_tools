#ifndef ARGX
#define ARGX

#include <map>

std::map<std::string, std::string> getArguments(int argc, char ** argv)
{
    std::map<std::string, std::string> args;
    int index = 1;
    while (index < argc)
    {
        std::string arg = argv[index];
        if (arg.rfind("-", 0) == 0 && index+1 < argc)
        {
            std::string value = argv[index+1];
            arg.erase(0,1);
            args[arg] = value;
            index++;
        }
        index++;
    }
    return args;
}

#endif /* ARGX */
