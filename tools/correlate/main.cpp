#include <cmath>
#include <fstream>
#include <sstream>
#include <iostream>

#include <correlate.h>

int main(int argc, char ** argv)
{
    bool stdout = false;
    if (argc > 1)
    {
        std::ifstream in(argv[1]);
        std::ofstream out;
        if (argc > 2){ out = std::ofstream(argv[2]);}
        else { stdout = true; }
        std::stringstream ss;
        double datum;
        std::string line;
        std::vector<double> data;
        while (std::getline(in, line))
        {
            ss = std::stringstream(line);
            while (ss >> datum) { data.push_back(datum); }
        }
        auto cor = autocorrelate(data);
        if (stdout)
        {
            for (uint64_t i = 0; i < cor.size()-1; i++) { std::cout << cor[i]/cor[0] << ", "; }
            std::cout << cor.back()/cor[0] << "\n";
        }
        else
        {
            for (uint64_t i = 0; i < cor.size()-1; i++) { out << cor[i]/cor[0] << ", "; }
            out << cor.back()/cor[0] << "\n";
        }
        return 0;
    }
    std::cout << "Please specify a data file as argument 1\n";
    return -1;
}