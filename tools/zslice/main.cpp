#include <chrono>
#include <string>
#include <fstream>
#include <filesystem>
#include <iostream>
#include <vector>
#include <thread>
#include <cmath>

#include <args.h>

const unsigned buffer = 65536;

bool progress = false;
double min = 0.0; double max = 1.0;
unsigned long threads = 0;
std::string fname = "CONFIG";

struct Atom
{
    std::string element;
    double x, y, z;
};

void processChunk
(
    std::streampos start,
    uint64_t startAtom,
    uint64_t readAtoms,
    std::string fname,
    uint8_t levcfg,
    std::vector<Atom> & slicedAtoms,
    uint64_t & done
)
{
    std::ifstream file(fname);
    file.seekg(start);

    uint64_t atomsRead = 0;
    slicedAtoms.reserve(buffer);
    std::string line;
    std::string name;
    uint64_t index;
    std::stringstream data;
    double x, y, z;
    while(atomsRead < startAtom)
    {
        for (uint8_t l = 0; l < levcfg+2; l++)
        {
            std::getline(file, line);
        }
        atomsRead++;
    }

    atomsRead = 0;
    while(std::getline(file, line) && atomsRead < readAtoms)
    {
        data = std::stringstream(line);
        data >> name >> index;

        std::getline(file, line);   
        data = std::stringstream(line);
        data >> x >> y >> z;
        if (z >= min && z <= max)
        {
            slicedAtoms.push_back({name, x, y, z});
            if (slicedAtoms.capacity() == slicedAtoms.size())
            {
                slicedAtoms.reserve(slicedAtoms.size()+buffer);
            }
        }

        for (uint8_t l = 0; l < levcfg; l++) { std::getline(file, line); }
        atomsRead++;
        done = atomsRead;
    }
    done = atomsRead;
}

bool allDone(std::vector<uint64_t> progress, std::vector<uint64_t> sizes, bool report = false)
{
    if (report)
    {
        for (uint8_t i = 0; i < progress.size(); i++)
        {
            std::cout << std::round((100.0*progress[i]/sizes[i])*100000.0)/100000.0;
            if (i < progress.size()-1){ std::cout << " | "; }
        }
        std::cout << "\n";
    } 
    for (uint8_t i = 0; i < progress.size(); i++)
    {
        if (progress[i] < sizes[i]) 
        { 
            return false; 
        }
    }
    return true;
}

int main(int argc, char ** argv)
{
    auto args = getArguments(argc, argv);

    if (args.find("c") != args.end())
    {
        fname = args["c"];
    }
    if (!std::filesystem::exists(fname))
    {
        std::cout << fname << " cannot be found, specify with -c\n";
        return 1;
    }

    if (args.find("min") != args.end())
    {
        min = std::stod(args["min"]);
    }

    if (args.find("max") != args.end())
    {
        max = std::stod(args["max"]);
    }

    if (args.find("progress") != args.end())
    {
        progress = true;
    }

    if (args.find("threads") != args.end())
    {
        threads = std::stoul(args["threads"]);
    }

    std::ifstream config(fname);
    std::ofstream slice("CONFIG-slice");

    std::string line;
    std::getline(config, line);
    slice << line << "\n";

    std::getline(config, line);
    uint64_t levcfg, imcon, atoms;
    std::stringstream data(line);
    data >> levcfg >> imcon >> atoms;
    if (levcfg > 2) { levcfg = 2; }   

    std::string cell;
    if (imcon != 0)
    {
        for (int i = 0; i < 3; i++)
        { 
            std::getline(config, line); 
            cell += line + "\n";
        }
    }

    auto start = config.tellg();
    uint64_t lines = atoms*(2+imcon);

    std::vector<Atom> slicedAtoms;

    if (threads == 0)
    {
        uint64_t done = 0;
        processChunk(start, 0, atoms, fname, levcfg, slicedAtoms, done);
    }
    else
    {
        uint64_t atomsPerThread = atoms / threads;
        uint64_t excess = atoms % threads;
        std::vector<uint64_t> threadAtoms(threads, 0);
        std::vector<uint64_t> offsets(threads, 0);
        uint64_t offset = 0;
        for (uint64_t i = 0; i < threads; i++)
        {
            offsets[i] = offset;
            threadAtoms[i] = atomsPerThread;
            if (excess > 0) { threadAtoms[i] += 1; excess -= 1; }
        }
        std::vector<std::vector<Atom>> chunks(threads, std::vector<Atom>());
        std::vector<uint64_t> done = std::vector<uint64_t>(threads, 0);

        for (uint64_t i = 0; i < threads; i++)
        {
            std::thread worker = std::thread
            (
                processChunk,
                start,
                offsets[i],
                threadAtoms[i],
                fname,
                levcfg,
                std::ref(chunks[i]),
                std::ref(done[i])
            );
            worker.detach();
        }
        while (!allDone(done, threadAtoms, progress)){ std::this_thread::sleep_for(std::chrono::milliseconds(1000)); }

        for (uint8_t i = 0; i < threads; i++)
        {
            for (const auto & atom : chunks[i]){ slicedAtoms.push_back(atom); }
        }
    }

    uint64_t natoms = slicedAtoms.size();
    slice << levcfg << " " << imcon << " " << natoms << "\n" << cell;
    config.clear();
    uint64_t index;
    std::string name;
    uint8_t width = 75;

    for (uint64_t atom = 0; atom < slicedAtoms.size(); atom++)
    {   
	uint8_t done = uint8_t(width*atom/float(natoms));
	uint8_t todo = width-done;
	if (progress && atom % buffer == 0)
        {
            std::cout << "["+std::string(done, '=')+std::string(todo,' ')+"]\r";
	        std::cout.flush();
        }
        slice << slicedAtoms[atom].element << " " << std::to_string(atom+1) << "\n";
        slice << slicedAtoms[atom].x << " " << slicedAtoms[atom].y << " " << slicedAtoms[atom].z << "\n";
    }
}  

