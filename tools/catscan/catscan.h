#ifndef CATSCAN
#define CATSCAN

#include <vector>
#include <fstream>
#include <sstream>
#include <string>
#include <iostream>
#include <cstdint>
#include <chrono>
#include <cmath>
#include <map>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image_write.h>

#include <config.h>
#include <args.h>

std::map<uint64_t, std::vector<uint64_t>> getZSlices
(
    const std::vector<CONFIG::Site> & sites,
    double r,
    uint64_t depth,
    double minX,
    double maxX,
    double minY,
    double maxY,
    double minZ,
    double maxZ
)
{
    std::map<uint64_t, std::vector<uint64_t>> zslices;

    for (uint64_t i = 0; i < depth; i++)
    {
        zslices[i] = {};
    }

    double lz = maxZ - minZ + 2.0*r;

    if (depth == 0)
    {
        depth = std::floor(lz / r);
        std::cout << "Chosen z-slice width: " << depth << " = floor( " << lz << " / " << r << " )\n"; 
    }

    double invdz = double(depth) / lz;

    std::cout << lz << ", " << 1.0 / invdz << "\n";

    for (const CONFIG::Site & s : sites)
    {
        zslices[uint64_t( std::floor((s.z - minZ + r) * invdz) )].push_back(s.index);
    }

    uint64_t maxOccupancy = 0;
    for (uint64_t i = 0; i < depth; i++)
    {
        maxOccupancy = std::max(maxOccupancy, zslices[i].size());
    }

    std::cout << "z-slice occupancy: \n";

    auto tic = std::chrono::high_resolution_clock::now();

    for (uint64_t i = 0; i < depth; i++)
    {
        double occupancy = double(zslices[i].size()) / double(maxOccupancy);
        uint8_t pips = std::floor(70*occupancy);
        std::cout << " ";
        for (uint8_t p = 0; p < pips; p++)
        {
            std::cout << "-";
        }
        std::cout << "\n";
    }

    auto toc = std::chrono::high_resolution_clock::now();
    auto taken = std::chrono::duration<double>(toc-tic).count();
    std::cout << "took: " << taken << "s\n";

    return zslices;
}

void processZslice
(
    uint64_t i,
    uint64_t width, 
    uint64_t height, 
    double minX, 
    double maxX, 
    double minY, 
    double maxY, 
    double r, 
    std::map<uint64_t, std::vector<uint64_t>> & zslices, 
    std::vector<CONFIG::Site> & sites
)
{
    std::cout << "processing: " << i << "\n";
    std::vector<uint8_t> pixels(width*height, 0);
    double lx = maxX - minX + 2.0*r;
    double dx = lx / double(width);
    double ly = maxY - minY + 2.0*r;
    double dy = ly / double(height);

    for (uint64_t px = 0; px < width; px++)
    {
        for (uint64_t py = 0; py < height; py++)
        {
            for (uint64_t a : zslices[i])
            {
                double rx = double(px)*dx + minX - sites[a].x;
                double ry = double(py)*dy + minY - sites[a].y;
                double d2 = rx*rx+ry*ry;
                
                if (d2 < 0.25*r*r)
                {
                    pixels[px*height+py] = 255;
                    break;
                } 
                
            }
        }
    }
    stbi_write_png(("zslice-"+std::to_string(i)+".png").c_str(), width, height, 1, pixels.data(), 0);
}

void processZslices
(
    std::vector<uint64_t> is,
    uint64_t width, 
    uint64_t height, 
    double minX, 
    double maxX, 
    double minY, 
    double maxY, 
    double r, 
    std::map<uint64_t, std::vector<uint64_t>> & zslices, 
    std::vector<CONFIG::Site> & sites
)
{
    for (uint64_t i : is)
    {
        processZslice
        (
            i,
            width,
            height,
            minX,
            maxX,
            minY,
            maxY,
            r,
            zslices,
            sites
        );
    }
}
#endif /* CATSCAN */
