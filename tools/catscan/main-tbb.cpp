#include <catscan.h>

#include <algorithm>
#include <execution>

int main(int argc, char ** argv)
{
    std::string path = "CONFIG";
    double r = 1.0;
    uint64_t width = 256;
    uint64_t height = 256;
    uint64_t depth = 0;

    auto args = getArguments(argc, argv);

    if (args.find("c") != args.end())
    {
        path = args["c"];
    }

    if (args.find("r") != args.end())
    {
        r = std::stod(args["r"]);
    }

    if (args.find("width") != args.end())
    {
        width = std::stoul(args["width"]);
    }

    if (args.find("height") != args.end())
    {
        height = std::stoul(args["height"]);
    }

    if (args.find("depth") != args.end())
    {
        depth = std::stoul(args["depth"]);
    }

    std::vector<CONFIG::Site> sites = CONFIG::parseCONFIG(path, r);

    double minX = sites[0].x;
    double maxX = sites[0].x;
    double minY = sites[0].y;
    double maxY = sites[0].y;
    double minZ = sites[0].z;
    double maxZ = sites[0].z;

    auto tic = std::chrono::high_resolution_clock::now();

    std::for_each
    (
        sites.cbegin(), 
        sites.cend(), 
        [&minX, &maxX, &minY, &maxY, &minZ, &maxZ](const CONFIG::Site & s)
        {
            minX = std::min(minX, s.x);
            maxX = std::max(maxX, s.x);
            minY = std::min(minY, s.y);
            maxY = std::max(maxY, s.y);
            minZ = std::min(minZ, s.z);
            maxZ = std::max(maxZ, s.z);
        }
    );

    minX -= r/2.0;
    maxX += r/2.0;
    minY -= r/2.0;
    maxY += r/2.0;
    minZ -= r/2.0;
    maxZ += r/2.0;

    auto toc = std::chrono::high_resolution_clock::now();
    auto taken = std::chrono::duration<double>(toc-tic).count();

    std::cout << "System bounds: [" 
              << minX << ", " << maxX << "]x[" 
              << minY << ", " << maxY << "]x[" 
              << minZ << ", " << maxZ << "]" 
              << " (taken " << taken << "s)\n"; 

    double lz = maxZ - minZ + 2.0*r;

    if (depth == 0)
    {
        depth = std::floor(lz / r);
        std::cout << "Chosen z-slice width: " << depth << " = floor( " << lz << " / " << r << " )\n"; 
    }

    std::map<uint64_t, std::vector<uint64_t>> zslices = getZSlices
    (
        sites, 
        r, 
        depth,
        minX,
        maxX,
        minY,
        maxY,
        minZ,
        maxZ
    );

    std::vector<uint64_t> work(depth);
    for (unsigned i = 0; i < depth; i++)
    {
        work[i] = i;
    }

    for_each
    (
        std::execution::par_unseq,
        work.cbegin(),
        work.cend(),
        [width, height, maxX, minX, maxY, minY, r, &zslices, &sites](uint64_t i)
        {
            auto tic = std::chrono::high_resolution_clock::now();
            processZslice(i, width, height, minX, maxX, minY, maxY, r, zslices, sites);
            auto toc = std::chrono::high_resolution_clock::now();
            auto taken = std::chrono::duration<double>(toc-tic).count();
            std::cout << "zslice " << std::to_string(i) << " density: took " << taken << "s\n";
        }
    );


    return 0;
}
