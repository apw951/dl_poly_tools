#include <catscan.h>

#include <algorithm>
#include <thread>

int main(int argc, char ** argv)
{
    std::string path = "CONFIG";
    double r = 1.0;
    uint64_t width = 256;
    uint64_t height = 256;
    uint64_t depth = 0;
    uint64_t threads = 1;

    auto args = getArguments(argc, argv);

    if (args.find("c") != args.end())
    {
        path = args["c"];
    }

    if (args.find("r") != args.end())
    {
        r = std::stod(args["r"]);
    }

    if (args.find("width") != args.end())
    {
        width = std::stoul(args["width"]);
    }

    if (args.find("height") != args.end())
    {
        height = std::stoul(args["height"]);
    }

    if (args.find("depth") != args.end())
    {
        depth = std::stoul(args["depth"]);
    }

    if (args.find("threads") != args.end())
    {
        threads = std::stoul(args["threads"]);
    }

    std::vector<CONFIG::Site> sites = CONFIG::parseCONFIG(path, r);

    double minX = sites[0].x;
    double maxX = sites[0].x;
    double minY = sites[0].y;
    double maxY = sites[0].y;
    double minZ = sites[0].z;
    double maxZ = sites[0].z;

    auto tic = std::chrono::high_resolution_clock::now();

    std::for_each
    (
        sites.cbegin(), 
        sites.cend(), 
        [&minX, &maxX, &minY, &maxY, &minZ, &maxZ](const CONFIG::Site & s)
        {
            minX = std::min(minX, s.x);
            maxX = std::max(maxX, s.x);
            minY = std::min(minY, s.y);
            maxY = std::max(maxY, s.y);
            minZ = std::min(minZ, s.z);
            maxZ = std::max(maxZ, s.z);
        }
    );

    minX -= r/2.0;
    maxX += r/2.0;
    minY -= r/2.0;
    maxY += r/2.0;
    minZ -= r/2.0;
    maxZ += r/2.0;

    auto toc = std::chrono::high_resolution_clock::now();
    auto taken = std::chrono::duration<double>(toc-tic).count();

    std::cout << "System bounds: [" 
              << minX << ", " << maxX << "]x[" 
              << minY << ", " << maxY << "]x[" 
              << minZ << ", " << maxZ << "]" 
              << " (taken " << taken << "s)\n"; 

    double lz = maxZ - minZ + 2.0*r;

    if (depth == 0)
    {
        depth = std::floor(lz / r);
        std::cout << "Chosen z-slice width: " << depth << " = floor( " << lz << " / " << r << " )\n"; 
    }

    std::map<uint64_t, std::vector<uint64_t>> zslices = getZSlices
    (
        sites, 
        r, 
        depth,
        minX,
        maxX,
        minY,
        maxY,
        minZ,
        maxZ
    );

    uint64_t workPerThread = std::min(uint64_t(std::ceil(double(depth) / double(threads))), depth);
    std::vector<std::vector<uint64_t>> work(threads);
    uint64_t thread = 0;
    uint64_t unassigned = depth;
    while (unassigned > 0)
    {
        work[thread] = {};
        for (unsigned j = 0; j < workPerThread; j++)
        {
	    if (unassigned == 0) { break; }
            work[thread].push_back(unassigned-1);
            unassigned--;
        }
        thread++;
	if (unassigned == 0) { break; }
    }

    std::vector<std::thread> workers;
    for (uint64_t i = 0; i < threads; i++)
    {	
        workers.push_back
        (
            std::thread
            (
                processZslices,
                work[i],
                width,
                height,
                minX,
                maxX,
                minY,
                maxY,
                r,
                std::ref(zslices),
                std::ref(sites)
            )
        );
    }

    for (std::thread & w : workers)
    {
        w.join();
    }

    return 0;
}
