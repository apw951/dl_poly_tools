#include <cuda.h>
#include "stdio.h"

#include <cuda_util.cuh>

__global__ void processZsliceCUDA
(
    float * image,
    float * sites,
    float lx,
    float dx,
    float minX,
    float ly,
    float dy,
    float minY,
    float r,
    unsigned nsites,
    unsigned width,
    unsigned height
)
{
    int px = threadIdx.x + blockDim.x*blockIdx.x;
    int py = threadIdx.y + blockDim.y*blockIdx.y;
    int site =  threadIdx.z + blockDim.z*blockIdx.z;

    if (px < width && py < height && site < nsites)
    {
        float x = sites[site*2];
        float y = sites[site*2+1];

        float rx = float(px)*dx + minX - x;
        float ry = float(py)*dy + minY - y;
        float d2 = rx*rx+ry*ry;

        float i = image[px * height + py];
        image[px * height + py] = min(i, d2 - 0.25*r*r);
    }
}
