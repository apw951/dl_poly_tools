#include "catscan.cuh"
#include "catscan.h"

int main(int argc, char ** argv)
{
    std::string path = "CONFIG";
    double r = 1.0;
    uint64_t width = 256;
    uint64_t height = 256;
    uint64_t depth = 0;

    uint64_t singleSlice = 0;
    bool doSingleSlice = false;

    uint64_t tx = 8;
    uint64_t ty = 8;
    uint64_t tz = 1;
    uint64_t batches = 1;

    auto args = getArguments(argc, argv);

    if (args.find("c") != args.end())
    {
        path = args["c"];
    }

    if (args.find("r") != args.end())
    {
        r = std::stod(args["r"]);
    }

    if (args.find("width") != args.end())
    {
        width = std::stoul(args["width"]);
    }

    if (args.find("height") != args.end())
    {
        height = std::stoul(args["height"]);
    }

    if (args.find("depth") != args.end())
    {
        depth = std::stoul(args["depth"]);
    }

    if (args.find("tx") != args.end())
    {
        tx = std::stoul(args["tx"]);
    }

    if (args.find("ty") != args.end())
    {
        ty = std::stoul(args["ty"]);
    }

    if (args.find("tz") != args.end())
    {
        tz = std::stoul(args["tz"]);
    }

    if (args.find("batches") != args.end())
    {
        batches = std::stoul(args["batches"]);
    }

    if (args.find("z") != args.end())
    {
        singleSlice = std::stoul(args["z"]);
        doSingleSlice = true;
    }

    std::vector<Site> sites = parseCONFIG(path, r);

    double minX = sites[0].x;
    double maxX = sites[0].x;
    double minY = sites[0].y;
    double maxY = sites[0].y;
    double minZ = sites[0].z;
    double maxZ = sites[0].z;

    auto tic = std::chrono::high_resolution_clock::now();

    for (const CONFIG::Site & s : sites)
    {
            minX = std::min(minX, s.x);
            maxX = std::max(maxX, s.x);
            minY = std::min(minY, s.y);
            maxY = std::max(maxY, s.y);
            minZ = std::min(minZ, s.z);
            maxZ = std::max(maxZ, s.z);
    }

    minX -= r/2.0;
    maxX += r/2.0;
    minY -= r/2.0;
    maxY += r/2.0;
    minZ -= r/2.0;
    maxZ += r/2.0;

    auto toc = std::chrono::high_resolution_clock::now();
    auto taken = std::chrono::duration<double>(toc-tic).count();

    std::cout << "System bounds: [" 
              << minX << ", " << maxX << "]x[" 
              << minY << ", " << maxY << "]x[" 
              << minZ << ", " << maxZ << "]" 
              << " (taken " << taken << "s)\n"; 

    double lz = maxZ - minZ + 2.0*r;

    if (depth == 0)
    {
        depth = std::floor(lz / r);
        std::cout << "Chosen z-slice width: " << depth << " = floor( " << lz << " / " << r << " )\n"; 
    }

    std::map<uint64_t, std::vector<uint64_t>> zslices = getZSlices
    (
        sites, 
        r, 
        depth,
        minX,
        maxX,
        minY,
        maxY,
        minZ,
        maxZ
    );

    double lx = maxX - minX + 2.0*r;
    double dx = lx / double(width);
    double ly = maxY - minY + 2.0*r;
    double dy = ly / double(height);

    std::vector<std::vector<uint8_t>> images;

    std::cout << "Processing zslices with (" << tx << ", " << ty << ", " << tz << ") threads per CUDA block\n"; 

    for (auto slice : zslices)
    {

        if (doSingleSlice && slice.first != singleSlice)
        {
            continue;
        }

        std::cout << "Processing slice " << std::to_string(slice.first) 
                  << ", occupancy: " << slice.second.size() 
                  << " using " << batches << " batches" << "\n";

        if (slice.second.size() < 1)
        {
            std::vector<uint8_t> png(width*height, 0);
            images.push_back(png);
            continue;
        }

        uint64_t atomsPerBatch = float(slice.second.size()) / float(batches);

        std::cout << atomsPerBatch << " atoms per batch\n";

        std::vector<std::vector<uint64_t>> batchSites(batches);

        uint64_t unassigned = slice.second.size();

        for (uint64_t b = 0; b < batches; b++)
        {
            batchSites[b] = {};
            for (uint64_t i = 0; i < atomsPerBatch; i++)
            {
                batchSites[b].push_back(slice.second[unassigned-1]);
                unassigned--;
                if (unassigned <= 0)
                {
                    break;
                }
            }
            if (unassigned <= 0)
            {
                break;
            }
        }

        std::vector<float> image(width*height, 0.0);
        std::vector<float> xy(2*atomsPerBatch, 0.0);

        float * d_image;
        size_t memImage = width*height*sizeof(float);

        float * d_sites;
        size_t memSites = 2*atomsPerBatch*sizeof(float);

        int deviceCount = 0;
        cudaGetDeviceCount(&deviceCount);
        std::cout << "Found: " << deviceCount << " cuda devices\n";
        if (deviceCount == 0)
        {
            std::cout << "" 
                      << "\n\n###################################\n\n"
                      <<     "ERROR no cuda enabled devices found\n\n"
                      <<     "###################################\n\n";

            return -1;
        }

        struct cudaDeviceProp props;
        cudaGetDeviceProperties(&props, 0);

        std::cout << "Selecting device 0\n";
        std::cout << "|  " << props.name << "\n";
        std::cout << "|  " << props.memoryClockRate / 1024 << " MHz Mem Clock\n";
        std::cout << "|  " << 2.0*props.memoryClockRate*(props.memoryBusWidth/8)/1.0e6 << " GB/s Peak bandwidth\n";
        std::cout << "|  " << float(props.totalGlobalMem)/(1024.0*1024.0*1024.0) << " Gb Global Memory\n"; 
        cudaSetDevice(0);

        cudaMalloc(&d_sites, memSites);
        cudaMalloc(&d_image, memImage);

            cudaMemcpy(d_image, image.data(), memImage, cudaMemcpyHostToDevice);

            CUDAOK();

            for (uint64_t batch = 0; batch < batches; batch++)
            {

                if (batchSites[batch].size() <= 0)
                {
                    continue;
                }
                
                auto tic = std::chrono::high_resolution_clock::now();
                std::cout << "|  processing batch " << batch << "\n";

                unsigned i = 0;
                for (uint64_t s : batchSites[batch])
                {
                    xy[i*2] = sites[s].x;
                    xy[i*2+1] = sites[s].y;
                    i += 1;
                }

                cudaMemcpy(d_sites, xy.data(), 2*batchSites[batch].size()*sizeof(float), cudaMemcpyHostToDevice);

                dim3 threadsPerBlock(tx, ty, tz);
                int bx = (width + threadsPerBlock.x - 1)/threadsPerBlock.x;
                int by = (height + threadsPerBlock.y - 1)/threadsPerBlock.y;
                int bz = (batchSites[batch].size() + threadsPerBlock.z - 1)/threadsPerBlock.z;
                dim3 blocksPerGrid(bx,by,bz);

                std::cout << "|  Using (" << bx << ", " << by << ", " << bz << ") blocks in the grid\n";

                CUDAOK();
                cudaDeviceSynchronize();

                processZsliceCUDA<<<blocksPerGrid, threadsPerBlock>>>
                (
                    d_image, 
                    d_sites,
                    lx,
                    dx,
                    minX,
                    ly,
                    dy,
                    minY,
                    r,
                    batchSites[batch].size(),
                    width,
                    height
                );

                cudaDeviceSynchronize();
                CUDAOK();

                auto toc = std::chrono::high_resolution_clock::now();
                std::cout << "|  took " << std::chrono::duration<double>(toc-tic).count() << "s\n";
            }

            cudaDeviceSynchronize();

            cudaMemcpy(&image[0], d_image, memImage, cudaMemcpyDeviceToHost);

        cudaFree(d_sites);
        cudaFree(d_image);
        CUDAOK();

        std::vector<uint8_t> png(image.size(), 0);

        for (unsigned i = 0; i < png.size(); i++)
        {
            if (image[i] < 0)
            {
                png[i] = 255;
            }
        }

        images.push_back(png);

    }

    for (unsigned i = 0; i < images.size(); i++)
    {
        stbi_write_png(("zslice-"+std::to_string(i)+".png").c_str(), width, height, 1, images[i].data(), 0);
    }

    
    return 0;
}

