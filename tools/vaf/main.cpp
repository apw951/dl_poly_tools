#include <cmath>
#include <fstream>
#include <sstream>
#include <iostream>
#include <map>
#include <set>

#include <correlate.h>

int main(int argc, char ** argv)
{
    bool stdout = false;
    bool progress = false;
    if (argc > 1)
    {
        std::ifstream in(argv[1]);
        std::ofstream out;
        if (argc > 2)
        {
            if (std::string(argv[2]) == std::string("progress")) { progress = true; stdout = true; }
            else { out = std::ofstream(argv[2]); }

            if (argc > 3) { if (std::string(argv[3]) == std::string("progress")) { progress = true; }}
        }
        std::stringstream ss;
        double datum;
        std::string line;

        // title, meta
        std::getline(in, line); std::getline(in, line);
        unsigned levcfg, imcon, natoms, frames;
        ss = std::stringstream(line);
        ss >> levcfg >> imcon >> natoms >> frames;
        if (levcfg < 1 && progress) { std::cout << "HISTORY has no velocities, using 1-point stencil.\n"; }

        std::vector<std::vector<double>> vx
        (
            natoms,
            std::vector<double>(frames, 0.0)
        );
        auto vy = vx;
        auto vz = vx;

        std::vector<std::string> atomSpecies(natoms, "");
        std::set<std::string> species;

        for (uint64_t t = 0; t < frames; t++)
        {
            // meta, cell
            for (unsigned i = 0; i < 1+imcon; i++)
            {
                std::getline(in, line);
            }
            for (uint64_t i = 0; i < natoms; i++)
            {
                // atom meta
                std::getline(in, line);
                if (t == 0)
                {
                    ss = std::stringstream(line);
                    ss >> atomSpecies[i];
                    species.insert(atomSpecies[i]);
                }

                std::getline(in, line);
                ss = std::stringstream(line);
                ss >> vx[i][t]
                   >> vy[i][t]
                   >> vz[i][t];

                if (levcfg > 0)
                {
                    std::getline(in, line);
                    ss = std::stringstream(line);
                    ss >> vx[i][t]
                    >> vy[i][t]
                    >> vz[i][t];
                }
                if (levcfg > 1) { std::getline(in, line); }
            }
            if (progress) { std::cout << "[Reading frame: " << t << "]\r"; std::cout.flush(); }
        }

        // Determine velocities
        if (levcfg == 0)
        {
            for (uint64_t i = 0; i < natoms; i++)
            {
                for (uint64_t t = 1; t < frames; t++)
                {
                    vx[i][t-1] = vx[i][t]-vx[i][t-1];
                    vy[i][t-1] = vy[i][t]-vy[i][t-1];
                    vz[i][t-1] = vz[i][t]-vz[i][t-1];
                }
                vx[i].pop_back();
                vy[i].pop_back();
                vz[i].pop_back();
            }
            frames--;
        }

        if (progress) { std::cout.flush(); }

        std::map<std::string, std::vector<double>> cor;
        for (const auto & s : species) { cor[s] = std::vector<double>(frames, 0.0); }

        for (uint64_t i = 0; i < natoms; i++)
        {
            if (progress) { std::cout << "[Correlating atom: " << i << "]\r"; std::cout.flush(); }
            auto corix = autocorrelate(vx[i]);
            auto coriy = autocorrelate(vy[i]);
            auto coriz = autocorrelate(vz[i]);
            auto & c = cor[atomSpecies[i]];
            for (uint64_t t = 0; t < c.size(); t++)
            {
                c[t] += corix[t]+coriy[t]+coriz[t];
            }
        }

        if (progress) 
        {
            std::cout.flush();
            std::cout << "\n";
        }

        if (progress)
        {
            std::cout << "Calculate VAF(s) for species: ";
            for (auto & s : species)
            {
                std::cout << s << " ";
            }
        }

        if (stdout)
        {
            for (auto & c : cor)
            {
                std::cout << c.first << "\n";
                for (uint64_t i = 0; i < c.second.size(); i++) { std::cout << c.second[i]/c.second[0] << ", "; }
                std::cout << c.second.back()/c.second[0] << "\n";
            }
        }
        else
        {
            for (auto & c : cor)
            {
                out << c.first << "\n";
                for (uint64_t i = 0; i < c.second.size(); i++) { out << c.second[i]/c.second[0] << ", "; }
                out << c.second.back()/c.second[0] << "\n";
            }
        }
        return 0;
    }
    std::cout << "Please specify a HISTORY file as argument 1\n";
    return -1;
}