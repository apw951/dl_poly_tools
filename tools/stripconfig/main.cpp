#include <string>
#include <fstream>
#include <filesystem>
#include <iostream>
#include <vector>

bool progress = false;
const uint8_t width = 40;

int main(int argc, char ** argv)
{
    std::string fname = "CONFIG";
 
    if (argc > 1) { fname = argv[1]; }
    if (!std::filesystem::exists(fname))
    {
        std::cout << fname << " cannot be found\n";
        return 1;
    }
    
    if (argc > 2) { if (std::string(argv[2]) == "progress") { progress = true; } }

    std::ifstream config(fname);
    std::ofstream stripped("CONFIG-stripped");

    std::string line;
    std::getline(config, line);
    stripped << line << "\n";    

    std::getline(config, line);
    uint64_t levcfg, imcon, natoms;
    std::stringstream data(line);
    data >> levcfg >> imcon >> natoms;
    if (levcfg == 0) { std::cout << "levcg 0, " << fname << " is minimal\n"; return 0; }
    if (levcfg > 2) { levcfg = 2; }   

    std::string cell;
    if (imcon != 0)
    {
        for (int i = 0; i < 3; i++)
        { 
            std::getline(config, line); 
            cell += line + "\n";
        }
    }

    stripped << 0 << " " << imcon << " " << natoms << "\n" << cell;

    uint64_t atom = 0;
    while(std::getline(config, line))
    {
        stripped << line << "\n";
        std::getline(config, line);   
        stripped << line << "\n";
        atom++;
        for (uint8_t i = 0; i < levcfg; i++){ std::getline(config, line); }
	if (progress && atom % 1024 == 0)
        { 
            uint8_t done = uint8_t(width*atom/float(natoms));
	    uint8_t todo = width-done;
            std::cout << "["+std::string(done, '=')+std::string(todo,' ')+"]\r";
	    std::cout.flush();
        }
	}
    
    if (progress) 
    {
        std::cout.flush();
        std::cout << "\n";
    }
}  
