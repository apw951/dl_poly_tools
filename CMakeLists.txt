cmake_minimum_required(VERSION 3.1)
project(DLPOLY_TOOLS)

option(WITH_TBB FALSE)
option(WITH_CUDA FALSE)

set(CMAKE_CXX_STANDARD 17)

if (NOT EXISTS ${CMAKE_BINARY_DIR}/CMakeCache.txt)
  if (NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE "Release" CACHE STRING "" FORCE)
    message(STATUS "CMAKE_BUILD_TYPE not speficied defaulting to ${CMAKE_BUILD_TYPE}")
  endif()
endif()

if (CMAKE_BUILD_TYPE STREQUAL "Release")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Ofast -Wall")
    SET(CMAKE_CUDA_FLAGS ${CUDA_NVCC_FLAGS} -O3 -DNDEBUG)
endif()

include_directories(common)

include(ExternalProject)

MESSAGE(STATUS "Trying to install fftw...")

ExternalProject_Add(project_fftw
  #GIT_REPOSITORY  https://github.com/FFTW/fftw3
  URL http://www.fftw.org/fftw-3.3.2.tar.gz
  PREFIX ${CMAKE_CURRENT_BINARY_DIR}/fftw
  CONFIGURE_COMMAND
${CMAKE_CURRENT_BINARY_DIR}/fftw/src/project_fftw/configure
--prefix=${CMAKE_CURRENT_BINARY_DIR}/fftw/install
  INSTALL_DIR ${CMAKE_CURRENT_BINARY_DIR}/fftw/install
)

add_library(fftw STATIC IMPORTED)
set(lib_fftw_name ${CMAKE_STATIC_LIBRARY_PREFIX}fftw3${CMAKE_STATIC_LIBRARY_SUFFIX})
set_target_properties(fftw PROPERTIES IMPORTED_LOCATION ${CMAKE_CURRENT_BINARY_DIR}/fftw/install/lib/${lib_fftw_name})

INCLUDE_DIRECTORIES(${CMAKE_CURRENT_BINARY_DIR}/fftw/install/include)

add_subdirectory(tools)
