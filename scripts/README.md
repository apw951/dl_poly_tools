### ```plot_statis```

- Plots a statis value to the terminal using ```gnuplot``` and ```termplotlib```.

E.g.

```shell
python plot_statis.py STATIS volum 
python plot_statis.py STATIS temp
python plot_statis.py STATIS stress_xy
```
 
### ```plot_rdf```

- Plots an RDFDAT to the terminal using ```gnuplot``` and ```termplotlib```

E.g.

```shell
python plot_rdf.py RDFDAT
```
