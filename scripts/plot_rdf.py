import termplotlib as tpl
from argparse import ArgumentParser
from pathlib import Path
from dlpoly.rdf import RDF


parser = ArgumentParser()
parser.add_argument("rdf", type=Path)
args = parser.parse_args()

rdf = RDF(args.rdf)
fig = tpl.figure()
[fig.plot(rdf.x, rdf.data[i, :, 0], width=80, height=40) for i in range(3)]

fig.show()

