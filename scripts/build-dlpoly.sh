#!/bin/bash
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

log ()
{
	echo -e "${GREEN}$1${NC}"
}

warn ()
{
	echo -e "${RED}$1${NC}"
}

apocrita="openmpi/4.1.4-gcc gcc/12.1.0 cmake"
scarf="GCC/11.3.0 CMake/3.24.3 OpenMPI/4.1.4"

modules="none"
commit="devel"
dir="build-"
dir_override=0
reuse=0
while [[ $# -gt 0 ]]; do
  case $1 in
    -m|--modules)
        modules=$2
        shift
        ;;
    -c|--commit)
        commit=$2
        shift
        ;;
    -b|--build-dir)
        dir=$2
        dir_override=1
        shift
        ;;
    -*|--*)
        echo "Unknown option $1"
        exit 1
        ;;
    *)
        POSITIONAL_ARGS+=("$1") 
        shift 
        ;;
  esac
done

if [ -d dl-poly ]; 
then
    warn "dl-poly directory already exists"
    read -p "Delete it? (Y/N): " confirm && [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] || reuse=1
fi

if [ $reuse -eq 0 ]; then
	rm -rf dl-poly
	log "Checking out commit/tag/branch $commit"
	git clone https://gitlab.com/ccp5/dl-poly.git
else
	log "Reusing dl-poly directory"
fi

if [ "$modules" != "none" ];
then 
	if [ "$modules" == "apocrita" ];
	then
		log "Loading modules $apocrita"
		module purge
		module load $apocrita
	elif [ "$modules" == "scarf" ];
	then
		log "Loading modules $scarf"
		module purge 
		module load $scarf
	else 
		log "Modules argument no recognised"
	fi
else
	log "Not loading any modules"
fi

if [ $dir_override -eq 0 ]; then
	dir="build-$commit"
fi

pushd .
cd dl-poly && git checkout $commit
if [ -d $dir ];
then
	warn "dl-poly/$dir directory already exists"
        read -p "Delete it? (Y/N): " confirm && [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] || rm -rf $dir && mkdir $dir && cd $dir 
else
	mkdir $dir
fi

cd $dir
log "Compiling into dl-poly/$dir"
cmake -DCMAKE_BUILD_TYPE=Release -DWITH_EXTRATIME=Off .. && make

if [ $? -eq 0 ]; then
	log "Compilation complete"
else
	warn "Compilation failed"
fi

if [ -d $USER/bin/DLPOLY.Z ];
then
        read -p "${USER}/bin/DLPOLY.Z exists. Relace it? (Y/N): " confirm && [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] || cp bin/DLPOLY.Z $USER/DLPOLY.Z
fi

popd
