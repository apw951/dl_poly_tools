import termplotlib as tpl
from argparse import ArgumentParser
from pathlib import Path
from dlpoly.statis import Statis
import numpy as np

def fuzzy_match(query, val):
    if query.lower() in val.lower():
        return True
    return False

parser = ArgumentParser()
parser.add_argument("statis", type=Path)
parser.add_argument("value", type=str)
parser.add_argument('-diff', default=False, action='store_true')
args = parser.parse_args()

statis = Statis(args.statis)
cols = {}
for (i, l) in enumerate(statis.labels):
    cols[l] = i

index = None
label = ""
for k in cols:
    if fuzzy_match(args.value, k):
        print(f"Plotting: {k}")
        index = cols[k]
        label = k
        break
if index == None:
    print(f"Could not find {args.value}. Possible (fuzzy matched) values are: ")
    for k in cols:
        print(k)
else:
    statis = Statis(args.statis)
    v = statis.data[:, index]
    if args.d:
        v = np.abs(v[1:]-v[0:-1])

    t = range(len(v))
    fig = tpl.figure()
    fig.plot(t, v, width=80, height=40, label=label)
    fig.show()

